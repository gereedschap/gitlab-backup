import os

import requests


def main() -> None:
    user = os.environ["GITLAB_USER_LOGIN"]
    api_v4_url = os.environ["CI_API_V4_URL"]
    print(f"[*] Getting projects for user '{user}'...")

    headers = {}
    if private_token := os.getenv("GL_TOKEN", ""):
        headers["Private-Token"] = private_token
    else:
        print("[-] GL_TOKEN not set. Will only download Public projects.")

    r = requests.get(
        headers=headers,
        url=f"{api_v4_url}/users/{os.environ['GITLAB_USER_LOGIN']}/projects",
    )

    projects = r.json()

    for project in projects:
        project_id = project["id"]
        project_slug = project["path_with_namespace"].replace("/", "__")
        print(f"[*] Downloading {project_slug} with id {project_id}...")
        r = requests.get(
            headers=headers,
            url=f"{api_v4_url}/projects/{project_id}/repository/archive.zip",
        )
        with open(f"/target/{project_slug}.zip", "wb") as file_stream:
            file_stream.write(r.content)


if __name__ == "__main__":
    main()

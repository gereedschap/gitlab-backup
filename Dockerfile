ARG PYVERSION=3.9-slim
FROM python:$PYVERSION

WORKDIR /app

COPY . .

RUN pip install .

CMD ["python", "-m", "gitlab_backup"]
